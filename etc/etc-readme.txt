                          ====================
                            Utility Settings
                          ====================

This folder contains settings for utilities used in the development of JGraphT. 
If you think of making source contribution you will find them helpful. However, 
you don't have to use these utilities if you don't want to. You can simply 
email me your source files and I'll do the rest. The authorship credits will, 
of course, remain yours.


-----------------------
checkstyle-settings.xml
-----------------------

A setting file for Checkstyle - a great Java style checker!

JGraphT project uses Checkstyle to verify that the code adhers to its coding
conventions. Checkstyle also verifies that the Javadoc is complete and 
accurate. Checkstyle comes in place of a long boring document of coding 
conventions. Checkstyle will only tell you where your code does not adhere to 
the coding conventions, and how it should be fixed. If your code adheres it 
won't bother you.

You can find more about Checkstyle at http://checkstyle.sourceforge.net.


-------------------
jalopy-settings.xml
-------------------

A setting file for Jalopy - a great Java source code formatter!

All JGraphT source file are formatted using these settings. It helps to keep
the code tidy with one consistent look. It also saves time during development. 
It is recommanded that you run Jalopy before Checkstyle - it will save you
messages regarding the formatting issues.

You can find more about Jalopy at http://jalopy.sourceforge.net.


Regards,

Barak Naveh (barak_naveh@users.sourceforge.net)
JGraphT Project Lead
