<!-- 
/* ==========================================
 * JGraphT : a free Java graph-theory library
 * ==========================================
 *
 * Project Info:  http://jgrapht.sourceforge.net/
 * Project Lead:  Barak Naveh (http://sourceforge.net/users/barak_naveh)
 *
 * (C) Copyright 2003-2004, by Barak Naveh and Contributors.
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */
/* ~~~~~~~~~
 * build.xml
 * ~~~~~~~~~
 * (C) Copyright 2003-2004, by Barak Naveh and Contributors.
 *
 * Original Author:  Barak Naveh
 * Contributor(s):   John V. Sichi
 *
 * $Id: build.xml,v 1.28 2005/07/17 05:33:21 perfecthash Exp $
 *
 * Changes
 * ~~~~~~~
 * 24-Jul-2003 : Initial revision (BN);
 * 15-Aug-2003 : Used wildcards instead of enumerating files(JVS);
 * 15-Aug-2003 : Added checkstyle task (JVS);
 * 15-Aug-2003 : Used JUnit batch test feature (JVS);
 * 15-Aug-2003 : Added jalopy task (JVS);
 * 03-Sep-2003 : Added etags support (JVS);
 * 24-Feb-2004 : Removed duplicates and cleaned code (BN);
 * 06-Mar-2004 : Added tasks for building a release by CVS tag (BN);
 * 13-Oct-2004 : Added task for building experimental package (MB);
 *
 */
-->

<!--
/**
 * An ANT build file for JGraphT (use Ant 1.6.0 and above).
 * Before running this script, make sure that the libraries used by JGraphT are
 * found under ./lib directory. This script will create directories ./build 
 * and ./testbuild for generated classfiles. These can be removed at any time 
 * via the 'clean' target.  A temporary ./distribution directory is created 
 * when building a distribution. A ./javadoc directory is created for the 
 * Javadoc HTML pages. 
 *
 * This script was originally based on a nice script written by David Gilbert.
 *
 * @author Barak Naveh
 * @author John V. Sichi
 *
 * @since 24-Jul-2003
 */
-->

<project name="jgrapht" default="all" basedir=".">

    <property name="latest.version.name"  value="0.6.0" />
    
    <!-- CVS tags and their respective version names -->
    <property name="ver_0_6_0.name"       value="0.6.0" />
    <property name="ver_0_5_3.name"       value="0.5.3" />
    <property name="ver_0_5_2.name"       value="0.5.2" />

    <!-- Initialization. -->
    <target name="init.local" unless="cvstag">
        <property name="checkout.dir"     value="checkout" />
        <property name="project.version"  value="${latest.version.name}-local" />
    </target>
    
    <target name="init.cvs" if="cvstag">
        <property name="checkout.dir"     value="${cvstag}" />
        <!-- A dereferencing heck to set project.version=${${cvstag}.name} -->
        <!-- To be simplified if future Ant versions will support it directly -->
        <echo file="project.version.tmp">project.version=$${${cvstag}.name}</echo>
        <property file="project.version.tmp" />
        <delete file="project.version.tmp" />
        <!-- project.version now has the dereferenced value of ${${cvstag}.name} -->
    </target>

    <target name="init" depends="init.local,init.cvs">
        <echo level="info">JAVA_HOME=${java.home}</echo>
        <property name="project.name"     value="jgrapht" />
        <property name="project.prefix"   value="${project.name}-${project.version}" />

        <!-- Directories -->
        <property name="build.dir"        value="${basedir}/build" />
        <property name="testbuild.dir"    value="${basedir}/testbuild" />
        <property name="expbuild.dir"     value="${basedir}/expbuild" />
        <property name="distrib.dir"      value="${basedir}/distribution"/>
        <property name="src.dir"          value="${basedir}/src"/>
        <property name="testsrc.dir"      value="${basedir}/testsrc"/>
        <property name="lib.dir"          value="${basedir}/lib"/>
        <property name="etc.dir"          value="${basedir}/etc"/>
        <property name="javadoc.dir"      value="${basedir}/javadoc"/>

        <!-- File names -->
        <property name="project.jar.name"    value="${project.prefix}.jar" />
        <property name="project.tar.name"    value="${project.prefix}.tar" />
        <property name="project.tar.gz.name" value="${project.prefix}.tar.gz" />
        <property name="project.zip.name"    value="${project.prefix}.zip" />
        <property name="build.file.name"     value="build.xml" />
        <property name="license.file.name"   value="license-LGPL.txt" />
        <property name="junit.jar.name"      value="junit.jar" />
        <property name="jgraph.jar.name"     value="jgraph.jar" />
        <property name="checkstyle.jar.name" value="checkstyle-all-3.3.jar" />

        <!-- Files -->
        <property name="project.jar"      value="${basedir}/${project.jar.name}" />
        <property name="project.tar"      value="${basedir}/${project.tar.name}" />
        <property name="project.tar.gz"   value="${basedir}/${project.tar.gz.name}" />
        <property name="project.zip"      value="${basedir}/${project.zip.name}" />
        <property name="junit.jar"        value="${lib.dir}/${junit.jar.name}"/>
        <property name="jgraph.jar"       value="${lib.dir}/${jgraph.jar.name}"/>
        <property name="checkstyle.jar"   value="${lib.dir}/${checkstyle.jar.name}"/>
        <property name="build.file"       value="${basedir}/${build.file.name}"/>
        <property name="checkstyle.settings" value="${etc.dir}/checkstyle-settings.xml"/>
        <property name="jalopy.settings"  value="${etc.dir}/jalopy-settings.xml"/>

        <!-- Classpath for building functional sources. -->
        <path id="build.classpath">
            <pathelement location="${junit.jar}"/>
            <pathelement location="${jgraph.jar}"/>
        </path>
        <property name="build.classpath" refid="build.classpath"/>

        <!-- Classpath for building tests. -->
        <path id="testbuild.classpath">
            <pathelement path="${build.classpath}"/>
            <pathelement location="${build.dir}"/>
        </path>
        <property name="testbuild.classpath" refid="testbuild.classpath"/>

        <!-- Classpath for running tests. -->
        <path id="test.classpath">
            <pathelement path="${testbuild.classpath}"/>
            <pathelement location="${testbuild.dir}"/>
        </path>
    </target>


    <!-- Compile the classes -->
    <target name="compile" depends="init"
                           description="Compiles the JGraphT source code.">

        <!-- create a build directory -->
        <mkdir dir="${build.dir}" />

        <!-- compile the source -->
        <javac srcdir="${src.dir}" destdir="${build.dir}" source="1.4">
            <classpath refid="build.classpath" />
            <include name="**/*.java"/>
            <exclude name="org/_3pq/jgrapht/experimental/**/*.java"/>
        </javac>
    </target>


    <!-- Compile the experimental classes -->
    <target name="exp.compile" depends="compile"
                           description="Compiles the experimental JGraphT source code.">

        <!-- create a build directory -->
        <mkdir dir="${expbuild.dir}" />

        <!-- compile the source -->
        <javac srcdir="${src.dir}" destdir="${expbuild.dir}" source="1.4">
            <classpath refid="testbuild.classpath" />
            <include name="org/_3pq/jgrapht/experimental/**/*.java"/>
        </javac>
    </target>


    <!-- Generate the runtime jar -->
    <target name="jar" depends="compile">
        <jar jarfile="${project.jar}" basedir="${build.dir}">
            <manifest>
                <attribute name="Main-Class" value="org._3pq.jgrapht.demo.JGraphAdapterDemo"/>
                <attribute name="Class-Path" value="lib/${jgraph.jar.name}"/>
            </manifest> 
        </jar>
    </target>


    <!-- Build all JUnit tests -->
    <target name="test.compile" depends="compile"
                                description="Builds all JUnit tests.">
        <!-- create a test build directory -->
        <mkdir dir="${testbuild.dir}" />

        <javac srcdir="${testsrc.dir}" destdir="${testbuild.dir}" source="1.4"
               debug="on">
            <classpath refid="testbuild.classpath" />
            <include name="org/_3pq/jgrapht/**"/>
            <exclude name="org/_3pq/jgrapht/experimental/**"/>
        </javac>
    </target>

    <!-- Run all JUnit tests -->
    <target name="test" depends="test.compile"
                        description="Builds and runs all JUnit tests.">
        <echo level="info">
If you get a junit task failure, you need to add the junit.jar to
the classpath, possibly by copying it into the ANT_HOME/lib folder.
See http://ant.apache.org/manual/OptionalTasks/junit.html for more.
        </echo>
        <junit printsummary="no" fork="yes" 
                           haltonerror="true" haltonfailure="true">
            <classpath refid="test.classpath"/>
            <formatter type="brief" usefile="no" />
            <batchtest>
                <fileset dir="${testsrc.dir}">
                    <include name="**/*Test.java"/>
                    <exclude name="**/Abstract*Test.java"/>
                    <exclude name="**/DefaultEdgeTest.java"/>
                    <exclude name="org/_3pq/jgrapht/experimental/**/*.java"/>
                </fileset>
            </batchtest>
        </junit>
    </target>

    <!-- Run checkstyle on source code -->
    <!-- TODO:  once code is all clean, use as a dependency for "all" task -->
    <target name="checkstyle" depends="init"
                           description="Runs checkstyle on all source code.">
        <taskdef resource="checkstyletask.properties" 
                 classpath="${checkstyle.jar}"/>
        <checkstyle config="${checkstyle.settings}">
            <fileset dir="${src.dir}">
                <include name="**/*.java"/>
                <exclude name="org/_3pq/jgrapht/experimental/**/*.java"/>
            </fileset>
            <fileset dir="${testsrc.dir}">
                <include name="**/*.java"/>
                <exclude name="org/_3pq/jgrapht/experimental/**/*.java"/>
            </fileset>
        </checkstyle>
    </target>

    <!-- Run jalopy on source code                                         -->
    <!-- NOTE:  requires manual installation of .jars in lib/jalopy        -->
    <target name="jalopy" depends="compile"
            description="Runs jalopy on all source code (see WARNING comment).">
        <taskdef name="jalopy" 
                 classname="de.hunsicker.jalopy.plugin.ant.AntPlugin">
            <classpath>
                <fileset dir="${basedir}/lib/jalopy">
                    <include name="*.jar"/>
                </fileset>
            </classpath>
        </taskdef>
        <jalopy backup="true" convention="${jalopy.settings}"
                classpathref="testbuild.classpath" history="FILE">
            <fileset dir="${basedir}">
                <include name="src/**/*.java"/>
                <include name="testsrc/**/*.java"/>
            </fileset>
        </jalopy>
    </target>

    <!-- Generate the JGraphT JavaDoc HTML pages -->
    <target name="javadoc" depends="init" description="Creates the Javadocs.">
        <delete dir="${javadoc.dir}" />
        <mkdir dir="${javadoc.dir}" />
        <javadoc destdir="${javadoc.dir}" 
                 access="protected" 
                 windowtitle="JGraphT : a free Java graph library"
                 overview="${src.dir}/overview.html">
            <classpath refid="build.classpath" />
            
            <packageset dir="${src.dir}" defaultexcludes="yes">
                <include name="**"/>
                <exclude name="org/_3pq/jgrapht/experimental/**"/>
            </packageset>
        </javadoc>
    </target>


    <!-- Prepares a distribution directory of JGraphT -->
    <target name="prepare.distribution" depends="jar,javadoc">
    
        <!-- make a temporary distribution directory -->
        <mkdir dir="${distrib.dir}" />

        <!-- add README -->
        <copy file="${basedir}/README.html" tofile="${distrib.dir}/README.html" />

        <!-- add license -->
        <copy file="${basedir}/${license.file.name}" tofile="${distrib.dir}/${license.file.name}" />

        <!-- add runtime jar file -->
        <copy file="${project.jar}" tofile="${distrib.dir}/${project.jar.name}" />
    
        <!-- add source files -->
        <copy todir="${distrib.dir}/src">
            <fileset dir="${src.dir}">
                <include name="overview.html"/>
                <include name="**/*.java"/>
                <include name="**/package.html"/>
                <exclude name="org/_3pq/jgrapht/experimental/**"/>
            </fileset>
        </copy>

        <!-- add source files of unit tests -->
        <copy todir="${distrib.dir}/testsrc">
            <fileset dir="${testsrc.dir}">
                <include name="**"/>
                <exclude name="org/_3pq/jgrapht/experimental/**"/>
            </fileset>
        </copy>

        <!-- add lib files -->
        <copy todir="${distrib.dir}/lib">
            <fileset dir="${lib.dir}">
                <include name="*.jar"/>
                <include name="lib-readme.txt"/>
                <exclude name="${checkstyle.jar.name}"/>
            </fileset>
        </copy>

        <!-- add ant build file (this file) -->
        <copy file="${build.file}" tofile="${distrib.dir}/${build.file.name}" />

        <!-- add etc files -->
        <copy todir="${distrib.dir}/etc">
            <fileset dir="${etc.dir}">
                <include name="checkstyle-settings.xml"/>
                <include name="etc-readme.txt"/>
                <include name="jalopy-settings.xml"/>
            </fileset>
        </copy>

        <!-- add Javadoc files -->
        <copy todir="${distrib.dir}/javadoc">
            <fileset dir="${javadoc.dir}"/>
        </copy>
    </target>
    

    <!-- Packages a .zip distribution for JGraphT -->
    <target name="zip" depends="prepare.distribution">

        <!-- convert end-of-line characters in text files to PC style -->
        <fixcrlf srcdir="${distrib.dir}/src"     eol="crlf" eof="remove" />
        <fixcrlf srcdir="${distrib.dir}/testsrc" eol="crlf" eof="remove" />
        <fixcrlf srcdir="${distrib.dir}/etc"     eol="crlf" eof="remove" />
        <fixcrlf srcdir="${distrib.dir}"         eol="crlf" eof="remove" 
                 includes="${build.file.name},${license.file.name}"/>

        <!-- make the zip file -->
        <zip zipfile="${project.zip}">
             <zipfileset dir="${distrib.dir}" prefix="${project.prefix}" />
        </zip>
    </target>


    <!-- Packages a .tar.gz distribution for JGraphT -->
    <target name="targz" depends="prepare.distribution">

        <!-- convert end-of-line characters in text files to Unix style -->
        <fixcrlf srcdir="${distrib.dir}/src"     eol="lf" eof="remove" />
        <fixcrlf srcdir="${distrib.dir}/testsrc" eol="lf" eof="remove" />
        <fixcrlf srcdir="${distrib.dir}/etc"     eol="lf" eof="remove" />
        <fixcrlf srcdir="${distrib.dir}"         eol="lf" eof="remove" 
                 includes="${build.file.name},${license.file.name}"/>

        <!-- make the tar.gz file -->
        <tar tarfile="${project.tar}">
             <tarfileset dir="${distrib.dir}" prefix="${project.prefix}" />
        </tar>
        <gzip zipfile="${project.tar.gz}" src="${project.tar}" />
        <delete file="${project.tar}" />
    </target>
    

    <!-- Clean outputs from previous build -->    
    <target name="clean" depends="init"
            description="Cleans all outputs from previous builds">
        <delete dir="${distrib.dir}" />
        <delete dir="${build.dir}" />
        <delete dir="${expbuild.dir}" />
        <delete dir="${testbuild.dir}" />
        <delete dir="${javadoc.dir}" />
        <delete file="${project.jar}" />
        <delete file="${project.zip}" />
        <delete file="${project.tar.gz}" />
    </target>


    <!-- ALL -->
    <target name="all" depends="clean,compile,test,zip,targz"
            description="Cleans, compiles, tests and creates distribution files (.zip and .tar.gz).">
            
        <!-- Clean unnecessary outputs from this build -->    
        <delete dir="${distrib.dir}" />
        <delete dir="${javadoc.dir}" />
        <delete file="${project.jar}" />
    </target>


    <!-- generate Emacs tags -->
    <fileset dir="${basedir}" id="fileset.taggable">
        <include name="src/**/*.java"/>
        <include name="testsrc/**/*.java"/>
    </fileset>
    <target name="etags" 
            depends="init" 
            description="Generates TAGS file for Emacs">
        <property name="file.taggable.tmp" 
            location="${basedir}/taggedfiles"/>
        <antcall target="etags.find_taggable_files"/>
        <exec dir="${basedir}" executable="ctags">
            <arg line="-e"/>
            <arg line="-L taggedfiles"/>
        </exec>
        <delete file="${file.taggable.tmp}" quiet="true"/>
    </target>

    <!-- Internal helper for etags target -->
    <target name="etags.find_taggable_files">
        <concat destfile="${file.taggable.tmp}">replaceme</concat>
        <pathconvert property="files.taggable" 
          pathsep="${line.separator}"
          dirsep="/" refid="fileset.taggable" >
          <map from="${basedir}" to="." />
        </pathconvert>
        <replace file="${file.taggable.tmp}" 
          token="replaceme" value="${files.taggable}" />
        <fixcrlf srcDir="${basedir}" includes="taggedfiles"
          eol="lf" eof="remove" />
    </target>


    <!-- Creates a release according to a user-defined CVS tag -->
    <target name="release" depends="init,checkout,cvstag.missing" if="cvstag"
            description="Checks out and builds a release according to a CVS tag">
            
        <echo>Starting release build at ${checkout.dir}/${build.file.name}</echo>

        <ant dir="${checkout.dir}" antfile="${build.file.name}" 
             target="all" inheritall="false" inheritrefs="false">
            <property name="cvstag" value="${cvstag}"/>
        </ant>
    </target>


    <!-- Checks out a snapshot according to a user-defined CVS tag -->
    <target name="checkout" depends="init,cvstag.missing" if="cvstag"
            description="Checks out a snapshot from CVS according to a tag">
        <echo level="info">Checking out files of CVS tag: ${cvstag}</echo>
        
        <property name="project.cvsroot" 
                  value=":pserver:anonymous@cvs.sourceforge.net:/cvsroot/jgrapht" />
        <property name="cvspass" value=".cvspass" />

        <delete dir="${checkout.dir}" />
        <mkdir  dir="${checkout.dir}" />

        <echo file="${cvspass}" append="false">${project.cvsroot} A
        </echo>

        <cvs command="checkout" cvsroot="${project.cvsroot}" package="."
             tag="${cvstag}" dest="${checkout.dir}" passfile="${cvspass}" />
       
        <delete file="${cvspass}" />      
    </target>


    <!-- Outputs an error message if CVS tag is unspecified -->
    <target name="cvstag.missing" unless="cvstag">
        <echo level="error">Variable $${cvstag} must be specified for selected target.</echo>
    </target>

</project>
