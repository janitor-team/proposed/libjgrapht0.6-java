                          ==================
                            Library Folder
                          ==================

This folder contains jar files of libraries used by JGraphT:

--------------------------
jgraph.jar (version 5.6.1.1)
--------------------------

The runtime library of the JGraph project. The JGraph library is licensed under
the terms of the GNU Lesser General Public License (LGPL), as with JGraphT.

You can find out more about JGraph and/or download the latest version from 
http://www.jgraph.com. You will also find there the source code of the
JGraph library.

Note: the JGraph jar in this folder is the build for Java 1.4.

-------------------------
junit.jar (version 3.8.1)
-------------------------

The runtime library of the JUnit testing framework. The JUnit library is 
licensed under the terms of the IBM Common Public License. 

You can find out more about JUnit and/or download the latest version from 
http://www.junit.org. You will also find there the source code of the JUnit 
library.


----------------------
checkstyle-all-3.3.jar
----------------------

The build-time Checkstyle library executed from ant.  This is only
required during build, and does not need to be distributed.
Checkstyle is licensed under the terms of the GNU Lesser General
Public License.

You can find out more about Checkstyle and/or download the latest
version from http://checkstyle.sourceforge.net. You will also find
there the source code of Checkstyle.
